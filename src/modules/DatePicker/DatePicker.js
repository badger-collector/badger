import React from "react";

import MiniCalendar from "../MiniCalendar/MiniCalendar";

import styled from "styled-components";

const Wrapper = styled.div``;

const Row = styled.div`
    display: flex;
`;

class DatePicker extends React.Component {
    state = {
        open: true,
        date: ""
    };

    handleCalendarState = open => this.setState({ open });

    render() {
        return (
            <Wrapper>
                <Row>
                    <button onClick={() => this.handleCalendarState(true)}>
                        OPEN
                    </button>
                    {this.state.date ? <p>{this.state.date}</p> : ""}
                </Row>
                <Row>
                    {this.state.open ? (
                        <MiniCalendar
                            cb={this.handleCalendarState}
                            month="November"
                        />
                    ) : (
                            ""
                        )}
                </Row>
            </Wrapper>
        );
    }
}

export default DatePicker;
