import BadgerLogo from "../../assets/images/badger2.png";
import { IMAGES } from "../../assets/styles/sizes";

import styled from "styled-components";

const Logo = styled.div`
	height: ${props => (props.size ? props.size : IMAGES.logo)};
	width: ${props => (props.size ? props.size : IMAGES.logo)};
	border-radius: ${props => (props.size ? props.size : IMAGES.logo)};
	background-image: url(${props => (props.image ? props.image : BadgerLogo)});
	background-size: cover;
	margin: auto;
	border: ${props => (props.border ? `10px solid white` : "none")};
`;

export default Logo;
