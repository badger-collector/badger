import React from "react";

import { BUTTONS as COLORS } from "../../assets/styles/colors";
import styled from "styled-components";

const Wrapper = styled.button`
	background-color: ${props =>
		props.bgColor ? props.bgColor : COLORS.primary};
	height: 40px;
	width: ${props => (props.size ? props.size : "100%")};
	border: none;
	border-radius: 5px;
	color: ${COLORS.white};
	font-weight: bold;
	margin: 10px 0;

	&:hover {
		cursor: pointer;
	}
`;

const Button = ({ children, onClick, bgColor, size }) => (
	<Wrapper onClick={onClick} bgColor={bgColor} size={size}>
		{children}
	</Wrapper>
);

export default Button;
