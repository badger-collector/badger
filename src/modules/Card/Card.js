import styled from "styled-components";
import { CARDS as COLORS } from "../../assets/styles/colors";

const Card = styled.article`
	background-color: #c8d6e5;
	margin: 10px;
	padding: 1em;
	border-radius: 5px;
	color: ${COLORS.font};

	&:hover {
		/* transition: transform 0.2s; */
		cursor: pointer;
		/* transform: scale(1.005); */
	}
`;

export default Card;
