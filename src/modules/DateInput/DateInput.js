import React from "react";

import styled from "styled-components";
import Input from "../Input/Input";
import { input } from "../../assets/styles/sizes";

const Wrapper = styled.div``;

const Row = styled.div`
    display: flex;

    * {
        text-align: center;
    }
`;

const ValidationMessage = styled.p`
    font-size: 10px;
    color: red;
`;

class DateInput extends React.Component {
    state = {
        day: "",
        month: "",
        year: "",
        error: false
    };

    handleChange = (e, error) => {
        if (e) {
            this.setState({
                error: false,
                [e.target.name]: e.target.value
            });
        } else {
            this.setState({ error: true });
        }
    };

    render() {
        const { error } = this.state;
        return (
            <Wrapper>
                <Row className="input">
                    <Input
                        name="day"
                        type="number"
                        placeholder="DD"
                        min="1"
                        max="31"
                        maxLength="2"
                        size={input.small}
                        cb={this.handleChange}
                    />
                    <Input
                        name="month"
                        type="number"
                        placeholder="MM"
                        min="1"
                        max="12"
                        maxLength="2"
                        size={input.small}
                        cb={this.handleChange}
                    />
                    <Input
                        name="year"
                        type="number"
                        placeholder="YYYY"
                        min="1"
                        max={new Date().getUTCFullYear()}
                        maxLength="4"
                        size={input.small}
                        cb={this.handleChange}
                    />
                </Row>
                <ValidationMessage>
                    {error ? "The input is invalid" : ""}
                </ValidationMessage>
            </Wrapper>
        );
    }
}

export default DateInput;
