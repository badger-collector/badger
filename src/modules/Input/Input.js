import React from "react";

import { checkIfNumber } from "../../utils/stringManipulation";

import styled from "styled-components";
import { TEXT } from "../../assets/styles/colors";

const Wrapper = styled.div`
	height: 40px;
	width: ${props => (props.size ? props.size : "auto")};
	border-radius: ${props => (props.transparent ? "0px" : "5px")};
	padding: 3px 10px;
	margin: 10px 0;
	background-color: ${props => (props.transparent ? "transparent" : "white")};
	border-bottom: ${props => (props.transparent ? "1px solid #8080806e" : "")};

	input:-webkit-autofill,
	input:-webkit-autofill:hover,
	input:-webkit-autofill:focus,
	input:-webkit-autofill:active {
		-webkit-text-fill-color: ${props =>
			props.transparent ? TEXT.white : TEXT.black};
		-webkit-box-shadow: ${props => (props.transparent ? "none" : "white")};
		transition: background-color 5000s ease-in-out 0s;
		box-shadow: ${props => (props.transparent ? "none" : "white")};
	}
`;

const BaseInput = styled.input`
	border: none;
	width: 100%;
	height: 90%;
	background-color: ${props => (props.transparent ? "transparent" : "white")};
	color: ${props => (props.transparent ? TEXT.white : TEXT.dark)};

	&:focus {
		outline: none;
	}
	&::placeholder {
		color: ${TEXT.placeholder};
		opacity: 0.75;
	}
`;

const validateInput = (e, min, max, type, cb) => {
	let valid = true;
	if (type === "number") valid = checkIfNumber(e.target.value, min, max);
	if (!valid) {
		e.target.value = "";
		cb("", true);
	} else cb(e, false);
};

const Input = ({
	className,
	name,
	maxLength,
	placeholder,
	min,
	max,
	size,
	type,
	transparent,
	cb,
}) => (
	<Wrapper size={size} transparent={transparent}>
		<BaseInput
			className={className}
			name={name}
			placeholder={placeholder}
			onChange={e => validateInput(e, min, max, type, cb)}
			maxLength={maxLength}
			transparent={transparent}
			type={type}
		/>
	</Wrapper>
);

export default Input;
