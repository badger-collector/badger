import React from "react";

import { TEXT as COLORS } from "../../assets/styles/colors";
import { TEXT as SIZES } from "../../assets/styles/sizes";
import styled from "styled-components";

const Wrapper = styled.div``;

const Message = styled.p`
	color: ${props => (props.color ? props.color : COLORS.dark)};
	font-size: ${SIZES.small[10]};
`;

const ValidationMessage = ({ visible, children, color }) => (
	<Wrapper>
		{visible ? <Message color={color}>{children}</Message> : ""}
	</Wrapper>
);

export default ValidationMessage;
