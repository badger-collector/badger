import Default from "../../assets/images/badger4.png";

import styled from "styled-components";

const ProfilePicture = styled.div`
    background-image: url(${props => props.img ? props.img : Default});
    height: 75px;
    width: 75px;
    border-radius: 40px;
    background-position: center;
    background-size: contain;
    background-repeat: no-repeat;
    background-color: white;
`;

export default ProfilePicture;