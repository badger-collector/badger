import styled from "styled-components";

const Form = styled.section`
    padding: 50px;
    border-radius: 5px;
    background-color: ${props => (props.bgColor ? props.bgColor : "white")};
`;

export default Form;
