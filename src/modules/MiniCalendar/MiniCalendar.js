import React from "react";

import DaysGrid from "./DaysGrid/DaysGrid";

import { months, yearInterval, daysInMonth, getWeekday, weekdays } from "../../utils/stringManipulation";

import { main, text } from "../../assets/styles/colors";
import styled from "styled-components";

const Wrapper = styled.div`
    height: 250px;
    width: 250px;
    border: 1px solid black;
    position: fixed;
    background-color: ${main.blue};
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    color: ${text.white};
`;

const MonthYear = styled.div`
    height: 30px;
    border-bottom: 1px solid;
    display: flex;
`;

const Days = styled.div`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    padding: 5px;
`;

const Day = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;

    &:hover {
        transition: all 0.2s;
        background-color: ${main.dark};
        cursor: pointer;
        border-radius: 25px;
    }
`;

const Select = styled.select`
    background-color: transparent;
    border: none;
    cursor: pointer;
`;

const Month = styled(Select)`
    flex-grow: 1;
`;

const Weekdays = styled.div`
    display: flex;
    justify-content: space-evenly;
    height: 30px;
    align-items: center;
`;

class MiniCalendar extends React.Component {
    constructor(props) {
        super(props);

        this.date = new Date();

        this.state = {
            day: this.date.getDate(),
            month: this.date.getUTCMonth(),
            year: this.date.getUTCFullYear(),
        }
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
    }

    setWrapperRef = node => {
        this.wrapperRef = node;
    };

    handleClickOutside = event => {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            const { cb } = this.props;
            cb(false);
        }
    };

    printMonths = () => months.map(month => <option key={month.index} value={month.index}>{month.name}</option>);

    printYears = () => {
        const years = yearInterval(1990, this.date.getUTCFullYear());
        return years.map(year => <option key={year.index} value={year.year}>{year.year}</option>);
    }

    printDays = () => {
        const days = daysInMonth(this.state.month, this.state.year);
        const weekday = getWeekday(1, this.state.month, this.state.year);
        const calendar = [];
        for (let i = 0; i < days + weekday; i++) {
            if (i < weekday) calendar[i] = "";
            else calendar[i + weekday] = i - weekday + 1;;
        }
        return calendar.map((day, i) => <Day key={i} name="day" onClick={(e, day) => this.selectedDay(day)}>{day}</Day>);
    }

    printWeekdays = () => weekdays.map((day, i) => <div key={i}>{day.acronym}</div>)

    handleChange = (e) => {
        this.setState({
            [e.target.name]: parseInt(e.target.value)
        })
    }

    selectedDay = (day) => {
        console.log(day)
    }

    render() {
        return (
            <Wrapper ref={this.setWrapperRef}>
                <MonthYear>
                    <Month defaultValue={this.state.month} name="month" onChange={this.handleChange}>
                        {this.printMonths()}
                    </Month>
                    <Select defaultValue={this.state.year} name="year" onChange={this.handleChange}>
                        {this.printYears()}
                    </Select>
                </MonthYear>
                <Days>
                    <Weekdays>{this.printWeekdays()}</Weekdays>
                    <DaysGrid>
                        {this.printDays()}
                    </DaysGrid>
                </Days>
            </Wrapper>
        );
    }
}

export default MiniCalendar;
