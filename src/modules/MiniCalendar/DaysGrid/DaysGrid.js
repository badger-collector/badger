import React from "react";

import styled from "styled-components";

const Wrapper = styled.div`
    flex-grow: 1;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-template-rows: 1fr 1fr 1fr 1fr 1fr 1fr;
    grid-template-areas: ". . . . . . ." ". . . . . . ." ". . . . . . ." ". . . . . . ." ". . . . . . ." ". . . . . . .";
    grid-column-start: 2;
`;

const DaysGrid = ({ children }) => (
    <Wrapper>{children}</Wrapper>
);

export default DaysGrid;
