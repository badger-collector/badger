import styled from "styled-components";

const Separator = styled.div`
    border-bottom: ${props => (props.vertical ? "none" : "1px solid")};
    border-right: ${props => (!props.vertical ? "none" : "1px solid")};
    margin: 10px 5px;
`;

export default Separator;
