import styled from "styled-components";

const Main = styled.section`
	flex-grow: 1;
`;

export default Main;
