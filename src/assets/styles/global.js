import styled, { css } from "styled-components";

const NoLinkButton = styled.button`
	a {
		text-decoration: none;
		color: black;
	}
`;

const generateBoxShadow = (horizontal, vertical, blur, spread) => css`
	-webkit-box-shadow: ${horizontal} ${vertical} ${blur} ${spread}
		rgba(0, 0, 0, 0.75);
	-moz-box-shadow: ${horizontal} ${vertical} ${blur} ${spread}
		rgba(0, 0, 0, 0.75);
	box-shadow: ${horizontal} ${vertical} ${blur} ${spread} rgba(0, 0, 0, 0.75);
`;

export { NoLinkButton, generateBoxShadow };
