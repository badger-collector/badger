const COLORS = {
	neutral: {
		1: "#f9f9f9",
		2: "#f7f1e3",
		3: "#ecf0f1",
		4: "#cbcbcb",
		5: "#575556",
		6: "#8080806e",
	},
	blue: {
		1: "#34ace0",
		2: "#227093",
		3: "#225679",
	},
	yellow: {
		1: "#f1c40f",
		2: "#be9626",
		3: "#f39c12",
		4: "#cd6133",
	},
	red: {
		1: "#e55039",
		2: "#ff5252",
	},
	green: {
		1: "#1dd1a1",
		2: "#218c74",
		3: "#274325",
	},
	opacity: {
		blue: {
			1: "#34ace0bd",
			2: "#22709382",
		},
		yellow: {
			2: "#be96267d",
		},
		red: {
			1: "#e550394a",
		},
		green: {
			1: "#1dd1a178",
			2: "#218c7485",
			3: "#274325ab",
		},
	},
};

const MAIN = {
	white: COLORS.neutral[1],
	dark: COLORS.neutral[4],
	gray: COLORS.neutral[5],
	red: COLORS.red[1],
	blue: COLORS.blue[3],
	yellow: COLORS.yellow[3],
	placeholder: COLORS.neutral[6],
	backgroundColor: COLORS.neutral[3],
};

const TEXT = {
	white: MAIN.white,
	gray: MAIN.gray,
	dark: MAIN.dark,
	placeholder: MAIN.placeholder,
};

const BUTTONS = {
	white: MAIN.white,
	primary: MAIN.red,
	secondary: MAIN.blue,
	alternative: MAIN.yellow,
};

const HEADER = {
	background: MAIN.blue,
};

const FORMS = {
	link: BUTTONS.primary,
	light: TEXT.white,
	dark: TEXT.dark,
	buttons: {
		primary: BUTTONS.primary,
		secondary: BUTTONS.secondary,
		alternative: BUTTONS.alternative,
	},
	primaryBg: COLORS.opacity.blue[2],
	secondaryBg: COLORS.opacity.red[1],
};

const CARDS = {
	font: TEXT.white,
	background: MAIN.dark,
};

export { MAIN, TEXT, BUTTONS, HEADER, FORMS, CARDS };
