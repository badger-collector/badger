const PIXELS = {
	xsmall: {
		1: "1px",
		2: "2px",
		3: "3px",
		4: "4px",
		5: "5px",
	},
	small: {
		6: "6px",
		7: "7px",
		8: "8px",
		9: "9px",
		10: "10px",
	},
	medium: {
		11: "11px",
		12: "12px",
		13: "13px",
		14: "14px",
		15: "15px",
	},
	large: {
		16: "16px",
		17: "17px",
		18: "18px",
		19: "19px",
		20: "20px",
	},
	xlarge: {
		21: "21px",
		22: "22px",
		23: "23px",
		24: "24px",
		25: "25px",
	},
	xxlarge: {
		50: "50px",
		75: "75px",
		100: "100px",
		200: "200px",
		300: "300px",
	},
};

const TEXT = {
	small: {
		8: PIXELS.small[8],
		10: PIXELS.small[10],
		12: PIXELS.medium[12],
		14: PIXELS.medium[14],
	},
	medium: {
		16: PIXELS.large[16],
		18: PIXELS.large[18],
		20: PIXELS.large[20],
	},
	large: {
		22: PIXELS.xlarge[22],
		24: PIXELS.xlarge[24],
	},
};

const HEIGHT = {
	25: PIXELS.xlarge[25],
	75: PIXELS.xxlarge[75],
	200: PIXELS.xxlarge[200],
	300: PIXELS.xxlarge[300],
};

const WIDTH = {
	200: PIXELS.xxlarge[200],
	300: PIXELS.xxlarge[300],
};

const PADDING = {
	10: PIXELS.small[10],
	25: PIXELS.xlarge[25],
};

const MARGIN = {
	20: PIXELS.large[20],
	25: PIXELS.xlarge[25],
};

const MAIN = {
	padding: PADDING[25],
	margin: MARGIN[25],
};

const HEADER = {
	height: HEIGHT[75],
	padding: PADDING[10],
	logoutBtnSize: PIXELS.xxlarge[50],
};

const IMAGES = {
	logo: "80px",
};

const BUTTONS = {
	lobby: {
		fontSize: TEXT.medium[20],
		size: HEIGHT[200],
	},
	small: "50px",
	medium: "100px",
	large: "150px",
	full: "100%",
};

const INPUTS = {
	small: "50px",
	medium: "100px",
	large: "150px",
	xlarge: "200px",
	xxlarge: "250px",
};

const FORMS = {
	small: {
		width: WIDTH[200],
		margin: MARGIN[20],
	},
	medium: {
		width: WIDTH[300],
	},
	link: TEXT.small[8],
	headerFont: TEXT.large[24],
};

export { PADDING, MARGIN, MAIN, HEADER, IMAGES, INPUTS, BUTTONS, TEXT, FORMS };
