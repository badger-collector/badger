import AuthReducer from "./auth.reducer";
import { combineReducers } from "redux";

const allReducers = combineReducers({
    auth: AuthReducer
});

export default allReducers;
