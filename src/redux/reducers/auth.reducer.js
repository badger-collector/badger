import { AUTH } from "../actions/types";

const INITIAL_STATE = {
    isLogged: false,
    error: false,
    message: "",
    loading: false,
    user: {}
};

const authReducer = (state = INITIAL_STATE, action) => {
    console.log(action);
    switch (action.type) {
        case AUTH.LOGIN.START:
            return {
                ...state,
                isLogged: false,
                loading: true
            };
        case AUTH.LOGIN.SUCCESS:
            return {
                ...state,
                isLogged: true,
                loading: false,
                user: action.payload
            };
        case AUTH.LOGIN.FAIL:
            return {
                ...state,
                isLogged: false,
                error: action.error,
                message: action.message
            };
        case AUTH.LOGOUT.START:
            return {};
        case AUTH.LOGOUT.SUCCESS:
            return {};
        case AUTH.LOGOUT.FAIL:
            return {};
        case AUTH.REGISTER.START:
            return {
                ...state,
                isLogged: false,
                loading: true,
            };
        case AUTH.REGISTER.SUCCESS:
            return {
                ...state,
                isLogged: true,
                loading: false,
                user: action.payload
            };
        case AUTH.REGISTER.FAIL:
            return {
                ...state,
                isLogged: false,
                error: action.error,
                message: action.message
            };
        default:
            return state;
    }
};

export default authReducer;
