import { authRequests } from "../../services";
import { AUTH } from "./types";

const LOGIN_START = {
    type: AUTH.LOGIN.START
};

const LOGIN_SUCCESS = user => ({
    type: AUTH.LOGIN.SUCCESS,
    payload: user
});

const LOGIN_FAIL = error => ({
    type: AUTH.LOGIN.FAIL,
    payload: error
});

const login = (username, password, cb) => {
    return dispatch => {
        dispatch(LOGIN_START);

        authRequests.login(username, password).then(res => {
            if (res.success) {
                dispatch(LOGIN_SUCCESS(res.data));
                cb();
            } else {
                dispatch(LOGIN_FAIL(res.message));
            }
        })
        .catch(err => {
            dispatch(LOGIN_FAIL(err.message));
        })
    };
};

const REGISTER_START = {
    type: AUTH.REGISTER.START
};

const REGISTER_SUCCESS = user => ({
    type: AUTH.REGISTER.SUCCESS,
    payload: user
});

const REGISTER_FAIL = error => ({
    type: AUTH.REGISTER.FAIL,
    payload: error
});

const register = (info, cb) => {
    return dispatch => {
        dispatch(REGISTER_START);

        authRequests.register(info).then(res => {
            if (res.success) {
                dispatch(REGISTER_SUCCESS(res.data));
                cb();
            } else {
                dispatch(REGISTER_FAIL(res.message));
            }
        })
        .catch(err => {
            dispatch(REGISTER_FAIL(err.message));
        })
    };
};

export { login, register };
