const AUTH = {
    LOGIN: {
        START: "LOGIN_START",
        SUCCESS: "LOGIN_SUCCESS",
        FAIL: "FAIL"
    },
    LOGOUT: {
        START: "LOGOUT_START",
        SUCCESS: "LOGOUT_SUCCESS",
        FAIL: "LOGOUT_FAIL"
    },
    REGISTER: {
        START: "REGISTER_START",
        SUCCESS: "REGISTER_SUCCESS",
        FAIL: "REGISTER_FAIL"
    }
};

const USER = {
    LOBBY: {
        START: "LOBBY_START",
        SCHEDULE: "LOBBY_SCHEDULE",
        LOBBY_ERROR: "LOBBY_ERROR"
    }
};

export { AUTH, USER };
