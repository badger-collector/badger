import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import PrivateRoute from "./PrivateRoute/PrivateRoute";

import Portal from "./Portal/Portal";
import Login from "./Login/Login";
import Register from "./Register/Register";
import ErrorPage from "./ErrorPage/ErrorPage";

import styled from "styled-components";
import NotFound from "./../assets/images/404.png";
import { MAIN } from "../assets/styles/colors";

const Wrapper = styled.div`
	min-height: 100vh;
	display: flex;
	background-color: ${MAIN.backgroundColor};
`;

const PageNotFound = () => (
	<ErrorPage documentTitle="Page not found!" image={NotFound} />
);

const App = () => (
	<Wrapper>
		<Switch>
			<Route exact path="/" render={() => <Redirect to="/portal" />} />
			<Route path="/login" component={Login} />
			<Route path="/register" component={Register} />
			<PrivateRoute path="/portal" component={Portal} />
			<Route component={PageNotFound} />
		</Switch>
	</Wrapper>
);

export default App;
