import React from "react";

import Main from "../../modules/Main/Main";

import NotFound from "../../assets/images/404.png";
import styled from "styled-components";

const Wrapper = styled(Main)`
    padding: 0;
    display: flex;
    justify-content: space-evenly;
    max-height: 100%;
    background-image: url(${props => props.image ? props.image : NotFound});
    background-repeat: no-repeat;
    background-position: center;

    @media screen and (max-width: 500px) {
    background-size: 250px;
}
`;


const ErrorPage = ({ documentTitle, image }) => {
    document.title = documentTitle ? documentTitle : "There was an error!";

    return (<Wrapper image={image} />);
}

export default ErrorPage;
