import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import Home from "../Home/Home";
import Header from "../Header/Header";

import styled from "styled-components";
import { main } from "../../assets/styles/colors";
import Day from "../../assets/images/day2.jpg";

const Wrapper = styled.main`
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    /* background-image: url(${Day}); */
    background-size: cover;
`;

const Portal = () => {
	return (
		<Wrapper>
			<Header />
			<Switch>
				<Route
					exact
					path={"/portal"}
					render={() => <Redirect to="/portal/home" />}
				/>
				<Route path={"/portal/home"} component={Home} />
			</Switch>
		</Wrapper>
	);
};
export default Portal;
