import React from "react";

import Card from "../../modules/Card/Card";
import Main from "../../modules/Main/Main";

import styled from "styled-components";
import { MAIN } from "../../assets/styles/sizes";
import Lobby from "../Lobby/Lobby";

const Wrapper = styled(Main)`
	display: grid;
	grid-template-columns: 1fr 1fr 1fr;
	grid-template-rows: 1fr 1fr 1fr;
	grid-template-areas: "MainCard MainCard SideCard1" "MainCard MainCard SideCard2" "SideCard3 SideCard4 SideCard5";
	margin: ${MAIN.margin};
`;

const MainCard = styled(Card)`
	grid-template-columns: 1fr;
	grid-template-rows: 1fr;
	grid-template-areas: ".";
	grid-area: MainCard;
`;

const SideCard = styled(Card)`
	grid-area: ${props => props.areaName};
`;

const Home = () => {
	document.title = "Home";

	return (
		<Wrapper>
			<MainCard>
				<h3>Desafio da Semana</h3>
				<div>REBENTAR A ANILHA À PAULA</div>
				<div>PRÉMIO: NÃO PAGAM</div>
			</MainCard>
			<SideCard areaName="SideCard1">
				<Lobby />
			</SideCard>
			<SideCard areaName="SideCard2">FIFA</SideCard>
			<SideCard areaName="SideCard3">CADERNETA</SideCard>
			<SideCard areaName="SideCard4">TRUST POINTS</SideCard>
			<SideCard areaName="SideCard5">SHOP</SideCard>
		</Wrapper>
	);
};

export default Home;
