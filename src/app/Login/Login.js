import React from "react";
import { Redirect, Link } from "react-router-dom";

import { connect } from "react-redux";
import { auth } from "../../redux/actions";

import Button from "../../modules/Button/Button";
import Main from "../../modules/Main/Main";
import Form from "../../modules/Form/Form";
import Input from "../../modules/Input/Input";
import Logo from "../../modules/Logo/Logo";

import Night from "../../assets/images/night2.png";
import styled from "styled-components";

import { FORMS as SIZES } from "../../assets/styles/sizes";
import { FORMS as COLORS } from "../../assets/styles/colors";

const Wrapper = styled(Main)`
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	background-image: url(${Night});
	background-size: cover;
`;

const LoginForm = styled(Form)`
	width: ${SIZES.medium.width};

	div:last-child {
		margin-top: ${SIZES.medium.margin};
	}

	@media only screen and (max-width: 450px) {
		width: ${SIZES.small.width};
		background-color: transparent;
		padding: 0;
	}
`;

const RegisterLink = styled(Link)`
	color: ${COLORS.link};
	font-size: ${SIZES.link};
`;

const Welcome = styled.p`
	font-size: ${SIZES.headerFont};
	color: ${COLORS.light};
	text-align: center;
`;

class Login extends React.Component {
	state = {
		redirectToReferrer: false,
		username: "",
		password: "",
	};

	handleLogin = () => {
		const { username, password } = this.state;
		const { onLogin } = this.props;
		onLogin(username, password, () => {
			this.setState({ redirectToReferrer: true });
		});
	};

	handleChange = (e, error) => {
		if (error) this.setState({ error: true });
		else
			this.setState({
				[e.target.name]: e.target.value,
			});
	};

	render() {
		document.title = "Login";
		let { from } = this.props.location.state || {
			from: { pathname: "/home" },
		};
		let { redirectToReferrer } = this.state;

		if (redirectToReferrer) return <Redirect to={from} />;

		return (
			<Wrapper>
				<LoginForm bgColor={COLORS.primaryBg}>
					<Logo size="150px" />
					<Welcome>the badger welcomes you!</Welcome>
					<Input
						name="username"
						placeholder="username"
						cb={this.handleChange}
						transparent={true}
					/>
					<Input
						name="password"
						placeholder="password"
						cb={this.handleChange}
						transparent={true}
						type="password"
					/>
					<Button
						bgColor={COLORS.buttons.alternative}
						onClick={this.handleLogin}
					>
						login
					</Button>
					<RegisterLink to="/register">
						Don't have an account yet? Click here!
					</RegisterLink>
				</LoginForm>
			</Wrapper>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onLogin: (username, password, cb) => {
			dispatch(auth.login(username, password, cb));
		},
	};
};

export default connect(null, mapDispatchToProps)(Login);
