import React from "react";
import { Redirect } from "react-router-dom";

import Main from "../../modules/Main/Main";
import Form from "../../modules/Form/Form";
import ValidationMessage from "../../modules/ValidationMessage/ValidationMessage";
import Input from "../../modules/Input/Input";
import Separator from "../../modules/Separator/Separator";
import Button from "../../modules/Button/Button";

import Sunrise from "../../assets/images/sunrise2.jpg";
import * as colors from "../../assets/styles/colors";
import { FORMS as SIZES } from "../../assets/styles/sizes";
import { FORMS as COLORS } from "../../assets/styles/colors";
import styled from "styled-components";

import { auth } from "../../redux/actions";
import { connect } from "react-redux";

const Wrapper = styled(Main)`
	align-items: center;
	justify-content: center;
	display: flex;
	background-image: url(${Sunrise});
	background-size: cover;
`;

const RegisterForm = styled(Form)`
	width: ${SIZES.medium.width};

	@media only screen and (max-width: 450px) {
		width: ${SIZES.small.width};
		background-color: transparent;
		padding: 0;
	}
`;
const Row = styled.div``;

class Register extends React.Component {
	state = {
		redirectToReferrer: false,
		username: "",
		firstname: "",
		lastname: "",
		email: "",
		password: "",
		confirmPass: "",
		error: false,
	};

	goBack = () => this.setState({ redirectToReferrer: true });

	handleChange = (e, error) => {
		if (error) this.setState({ error: true });
		else
			this.setState({
				[e.target.name]: e.target.value,
			});
	};

	validatePassword = (password, confirmation) => password === confirmation;

	handleRegister = () => {
		const {
			username,
			firstname,
			lastname,
			email,
			password,
			confirmPass,
		} = this.state;
		const { onRegister } = this.props;
		if (this.validatePassword(password, confirmPass)) {
			const info = {
				username,
				password,
				firstname,
				lastname,
				email,
			};

			onRegister(info, () => {
				this.setState({ redirectToReferrer: true, error: false });
			});
		} else {
			this.setState({ error: true });
		}
	};

	render() {
		document.title = "Register";

		let { from } = this.props.location.state || {
			from: { pathname: "/login" },
		};
		let { redirectToReferrer } = this.state;

		if (redirectToReferrer) return <Redirect to={from} />;

		return (
			<Wrapper>
				<RegisterForm bgColor={COLORS.secondaryBg}>
					<Button bgColor={COLORS.buttons.secondary} onClick={this.goBack}>
						go back
					</Button>
					<Separator vertical={false} />
					<Row>
						<Input
							name="username"
							placeholder="username"
							cb={this.handleChange}
						/>
						<ValidationMessage>Input was invalid</ValidationMessage>
					</Row>
					<Separator vertical={false} />
					<Row>
						<Input
							name="firstname"
							placeholder="first name"
							cb={this.handleChange}
						/>
						<Input
							name="lastname"
							placeholder="last name"
							cb={this.handleChange}
						/>
						<ValidationMessage>Input was invalid</ValidationMessage>
					</Row>
					<Separator vertical={false} />
					<Row>
						<Input
							name="email"
							placeholder="email"
							cb={this.handleChange}
							type="email"
						/>
						<ValidationMessage>Input was invalid</ValidationMessage>
					</Row>
					<Separator vertical={false} />
					<Row>
						<Input
							name="password"
							placeholder="password"
							cb={this.handleChange}
							type="password"
						/>
						<Input
							name="confirmPass"
							placeholder="confirm your password"
							cb={this.handleChange}
							type="password"
						/>
						<ValidationMessage>Input was invalid</ValidationMessage>
					</Row>
					<Separator vertical={false} />
					<Button onClick={this.handleRegister} bgColor={COLORS.secondaryBtn}>
						submit
					</Button>
				</RegisterForm>
			</Wrapper>
		);
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onRegister: (info, cb) => {
			dispatch(auth.register(info, cb));
		},
	};
};

export default connect(null, mapDispatchToProps)(Register);
