import React from "react";

import ProfilePicture from "../../modules/ProfilePicture/ProfilePicture";

import { HEADER as SIZES } from "../../assets/styles/sizes";
import { HEADER as COLORS } from "../../assets/styles/colors";
import Logout from "../../assets/images/logout.svg";

import styled from "styled-components";

const Wrapper = styled.header`
	min-height: ${SIZES.height};
	width: 100%;
	background-color: ${COLORS.background};
`;

const Container = styled.div`
	padding: ${SIZES.padding};
	display: flex;
	justify-content: space-evenly;
	align-items: center;
`;

const LogoutButton = styled.button`
	width: ${SIZES.logoutBtnSize};
	height: ${SIZES.logoutBtnSize};
	border: none;
	background: none;
	padding: 0;

	&:hover {
		cursor: pointer;
	}
`;

const LogoutSvg = styled.img`
	height: 100%;
	width: 100%;
`;

const Header = () => (
	<Wrapper>
		<Container>
			<ProfilePicture />
			<LogoutButton>
				<LogoutSvg src={Logout} />
			</LogoutButton>
		</Container>
	</Wrapper>
);

export default Header;
