import styled from "styled-components";

import UnderConstruction from "../../assets/images/under-construction.png";

const PageUnderConstruction = styled.main`
    display: flex;
    width: 100%;
    justify-content: space-evenly;
    max-height: 100%;
    background-image: url(${UnderConstruction});
    background-repeat: no-repeat;
    background-position: center;
`;

export default PageUnderConstruction;
