import React from "react";

import styled from "styled-components";

import { BUTTONS as COLORS } from "../../assets/styles/colors";
import { BUTTONS as SIZES } from "../../assets/styles/sizes";

const Wrapper = styled.section`
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const LobbyBtn = styled.button`
	height: ${SIZES.lobby.size};
	width: ${SIZES.lobby.size};
	border-radius: 100px;
	background-color: ${COLORS.primary};
	font-size: ${SIZES.xlarge};
	color: ${COLORS.white};
	font-weight: bold;

	&:hover {
		transition: transform 0.2s;
		cursor: pointer;
		transform: scale(1.005);
	}
`;

const Lobby = () => (
	<Wrapper>
		<LobbyBtn>LOBBY</LobbyBtn>
	</Wrapper>
);

export default Lobby;
