import axios from "axios";

const BASE_URL = "http://localhost:4000/auth";

const login = (username, password) => {
    return axios
        .post(`${BASE_URL}/login`, {
            username,
            password
        })
        .then(res => res.data);
};

const register = data => {
    return axios.post(`${BASE_URL}/register`, {
        username: data.username,
        password: data.password,
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email
    })
    .then(res => res.data);
};

export { login, register };
