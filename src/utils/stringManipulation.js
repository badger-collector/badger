const months = [
    {
        index: 0,
        name: "January",
        acronym: "JAN",
    },
    {
        index: 1,
        name: "February",
        acronym: "FEB",
    },
    {
        index: 2,
        name: "March",
        acronym: "MAR",
    },
    {
        index: 3,
        name: "April",
        acronym: "APR",
    },
    {
        index: 4,
        name: "May",
        acronym: "MAY",
    },
    {
        index: 5,
        name: "June",
        acronym: "JUN",
    },
    {
        index: 6,
        name: "July",
        acronym: "JUL",
    },
    {
        index: 7,
        name: "August",
        acronym: "AUG",
    },
    {
        index: 8,
        name: "September",
        acronym: "SEP",
    },
    {
        index: 9,
        name: "October",
        acronym: "OCT",
    },
    {
        index: 10,
        name: "November",
        acronym: "NOV",
    },
    {
        index: 11,
        name: "December",
        acronym: "DEC",
    },
];

const weekdays = [
    {
        index: 0,
        name: "Sunday",
        acronym: "Sun"
    },
    {
        index: 1,
        name: "Monday",
        acronym: "Mon"
    },
    {
        index: 3,
        name: "Tuesday",
        acronym: "Tue"
    },
    {
        index: 4,
        name: "Wednesday",
        acronym: "Wed"
    },
    {
        index: 5,
        name: "Thursday",
        acronym: "Thu"
    },
    {
        index: 6,
        name: "Friday",
        acronym: "Fri"
    },
    {
        index: 7,
        name: "Saturday",
        acronym: "Sat"
    },
]

const yearInterval = (start, end) => {
    const years = [];
    let i = 0;
    while (start + i <= end) {
        years.push(
            {
                index: i,
                year: start + i
            }
        );
        i++;
    }
    return years;
}

const daysInMonth = (month, year) => new Date(year, month + 1, 0).getDate();

const getWeekday = (day, month, year) => new Date(year, month, day).getDay();

const checkIfBirthday = (day, month, year) => {
    if ((month < 1 || month > 12) && (year < 1900 || year > new Date().getUTCFullYear())) return false;
    const nDays = new Date(year, month, 0).getDate();
    if (day < 1 || day > nDays) return false;
    return true;
}

const checkIfNumber = (number, min, max) => {
    const pattern = /[0-9]+/;
    if (!pattern.test(number)) return false;
    if (!min && !max) return true;
    if (number < min) return false;
    if (number > max) return false;
    return true;
}

export {
    months,
    weekdays,
    yearInterval,
    daysInMonth,
    getWeekday,
    checkIfBirthday,
    checkIfNumber
}
