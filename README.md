Features to implement:
- Lista de todos os baits para jogos que foram feitos
- Factor de confiança para utilizadores
- Registo de utilizadores
- Sistema de badges
- Desafio semanal com recompensa de badge
- Caderneta de badges com recompensa de jantar

Tecnologias usadas:
- React
- Styled-components

Cenários de recompensa de badge:
- Aniversário
- Cumprimento do desafio semanal
- Maior factor de confiança ao final de 1 mês
